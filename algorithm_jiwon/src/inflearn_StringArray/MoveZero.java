package inflearn_StringArray;

public class MoveZero {
	
	public static void main(String[] args) throws Exception {
		int[] nums = {0, 3, 2, 0, 8, 5};
		int index = 0;
		
		for(int i=0; i<nums.length; i++) {
			if(nums[i] != 0) {
				nums[index] = nums[i];  // nums[0]=3, nums[1]=2, nums[8], nums[5]
				index++;
			}
		}
		
		while(index < nums.length) {  // 4<6
			nums[index] = 0;
			index++;
		}
		
		print(nums);
				
	}
	
	public static void print(int[] nums) {
		for(int i=0; i<nums.length; i++) {
			System.out.print(nums[i] + " ");
		}
	}
}
