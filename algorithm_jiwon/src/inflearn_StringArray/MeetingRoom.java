package inflearn_StringArray;

import java.util.Arrays;
import java.util.Comparator;
/* 1. 객체 Sorting
 * 2. curent.end > next.start -> false
 * */
class Interval {
	int start;
	int end;
	
	public Interval() {
		this.start = 0;
		this.end = 0;
	}
	
	public Interval(int start, int end) {
		this.start = start;
		this.end = end;
	}
}

public class MeetingRoom {
	
	public static void main(String[] args) {
		
		MeetingRoom mr = new MeetingRoom();
		
		Interval iv1 = new Interval(15, 20);
		Interval iv2 = new Interval(5, 10);
		Interval iv3 = new Interval(0, 30);
		
		
		Interval[] intervals = {iv1, iv2, iv3};
		
		System.out.println(mr.solve(intervals));
	}
	
	public boolean solve(Interval[] intervals) {
		// 1. null 체크
		if(intervals == null) {
			return false;
		}
		//print(intervals);  // 15, 
		
		// 2. 오름차순 sort
		Arrays.sort(intervals, Comp);
		//print(intervals);
		
		// 3. if(current.end > next.start)   -> false
		for(int i=1; i<intervals.length; i++) {
			if(intervals[i-1].end > intervals[i].start) {
				return false;
			} else {
				return true;
			}
		}
		
		return true;
	}
	
	Comparator<Interval> Comp = new Comparator<Interval>() {

		@Override
		public int compare(Interval o1, Interval o2) {
			return o1.start - o2.start;  // 오름차순 
		}
	};
	
	public static void print(Interval[] intervals) {
		for(int i=0; i<intervals.length; i++) {
			Interval in = intervals[i];
			System.out.println(in.start + " " + in.end);
		}
		System.out.println();
	}
	
} // class MeetingRoom
