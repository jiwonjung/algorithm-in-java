package basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class DataStructure_ArrayList {
//
	
	
	public static void main(String[] args) {
		List<Integer> arrList = new ArrayList<>();
		
		// 요소 추가
		arrList.add(50);
		arrList.add(40);
		arrList.add(30);
		arrList.add(20);
		arrList.add(10);
		
		// 출력 1
		System.out.print("출력1: ");
		for (int i = 0; i < arrList.size(); i++) {
			System.out.print(arrList.get(i) + " ");
		}
		System.out.println();
		
		// 요소 삭제
		arrList.remove(1);
		
		// 출력 2
		System.out.print("출력2: ");
		Iterator<Integer> iterator = arrList.iterator();
		while(iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		System.out.println();
		
		// 요소 오름차순 정렬
		Collections.sort(arrList);
		
		// 요소 변경
		arrList.set(0, 100);
		
		// 출력 3
		System.out.print("출력3: ");
		for (Integer i : arrList) {
			System.out.print(i + " ");
		}
		System.out.println();
		
		// 사이즈 출력
		System.out.println("사이즈: " + arrList.size());
	}
}
