package basic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class 정렬_Bubble {
	
	// 29 10 14 37 13
	public static void main(String[] args) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		int n = st.countTokens();
		
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		
		bubbleSort(arr);
	}
	
	public static void bubbleSort(int[] arr) {
		int tmp;
		int length = arr.length;
		
		// 버블정렬
		for (int i = length-1; i > 0; i--) {
			for(int j = 0; j < i; j++) {
				if(arr[j] > arr[j+1]) {
					tmp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = tmp;
				}
			}
		}
		
		// 출력
		for (int i : arr) {
			System.out.print(i + " ");
		}
	}
}
