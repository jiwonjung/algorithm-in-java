package basic;

import java.io.BufferedReader;
import java.io.InputStreamReader;

// 1234567을 입력받으면 결과는 1+2+3+4+5+6+7=28출력
public class 입력받은수를1의자리로나눈뒤합계   {
	public static void main(String[] args) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String input = br.readLine();
		
		int sum = 0; 
		
		for (int i = 0; i < input.length(); i++) {
			// sum += input.charAt(i) - '0';  // 방법 1
			sum += Character.getNumericValue(input.charAt(i));  //방법 2
		}
		
		System.out.println(sum);
	}
}
