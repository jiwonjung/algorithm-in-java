package basic;
/*어떤 숫자 n이 자신을 제외한 모든 약수들의 합과 같으면, 그 수를 완전수라고 한다. 
예를 들어 6은 6 = 1 + 2 + 3 으로 완전수이다.

6 = 1 + 2 + 3
12 is NOT perfect.
28 = 1 + 2 + 4 + 7 + 14
 * */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class 약수_완전수  {
	public static void main(String[] args) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		while(true) {
			int n = Integer.parseInt(br.readLine());
			
			if (n == -1)  {
				break;
			}
			
			solve(n);
		}
	}
	
	public static void solve(int n) {
		int[] arr = new int[n];
		int index = 0; 
		
		int sum = 0; 
		
		for(int i=1; i<n; i++) {
			if(n % i == 0) {
				arr[index++] = i;  // [0]=1, [1]=2, [2]=3   ... 그리고 인덱스는 3까지++되어있음 
				sum += i;  // 0+1=1 / 1+2=3 / 3+3=6
			}
		}
		
		if(n == sum) {
			System.out.print(n + " = ");
			for(int i=0; i<index; i++) {
				System.out.print(arr[i]);
				if(i+1 != index) {
					System.out.print(" + ");
				}
			}
			System.out.println();
		} else {
			System.out.println(n + " is NOT perfect.");
		}
		
	}
	
}
