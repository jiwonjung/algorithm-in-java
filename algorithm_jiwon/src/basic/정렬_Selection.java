package basic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class 정렬_Selection {
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int n = st.countTokens();
		
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		
		selectionSort(arr);
	}
	
	public static void selectionSort(int[] arr) {
		int tmp;
		int length = arr.length;
		
		int min = 0; 
		for(int i = 0; i < length-1; i++) {
			min = i;
			for (int j = i+1; j < length; j++) {
				if(arr[j] < arr[min]) {
					min = j;
				}
			}
			tmp = arr[i];
			arr[i] = arr[min];
			arr[min] = tmp;
		}
		
		
		// 출력 
		for (int i : arr) {
			System.out.print(i + " ");
		}
		
	}
}
