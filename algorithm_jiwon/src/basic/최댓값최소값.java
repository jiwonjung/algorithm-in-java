package basic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 배열에 저장된 자료중 가장 큰 값, 작은 값 찾아 출력 
public class 최댓값최소값 {
	// 70 100 34 567 3 60 
	public static void main(String[] args) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		int n = st.countTokens();
		
		int[] arr = new int[n];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		
		int max = 0; 
		int min = 100;
		
		for (int i = 0; i < arr.length; i++) {
			if(max <= arr[i]) {
				max = arr[i];  // 1 70 100 
			} else if (min >= arr[i]){
				min = arr[i];
			}
		}
		
		System.out.println("최대값: " + max);
		System.out.println("최소값: " + min);
	}	

}
