package basic;

public class Sum_1부터100까지 {
	
	public static void main(String[] args) {
		
		makeSum(1, 100);
		
	}

	public static void makeSum(int start, int end) {
		int sum = 0;
		
		for (int i = start; i <= end; i++) {
			sum += i;
		}
		System.out.println("sum: " + sum);
	}
}
