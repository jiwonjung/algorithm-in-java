package basic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 5개의 정수를 입력받아 그 중 홀수와 짝수의 개수를 구하여 출럭하는 프로그램
public class 홀수짝수개수 {
	
	// 3 46 4 100 46
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		int n = st.countTokens();
		
		// 배열에 담기
		int[] arr = new int[n];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		
		solve(arr);
		
	}
	
	public static  void solve(int[] arr) {
		int oddCnt = 0;
		int evenCnt = 0;
		
		for (int i = 0; i < arr.length; i++) {
			if(arr[i] % 2 == 0) {  // 짝수일 경우
				evenCnt++;
			} else { // 홀수일 경우
				oddCnt++;
			}
		}
		
		System.out.println("홀수: " + oddCnt);
		System.out.println("짝수: " + evenCnt);
	}

}
