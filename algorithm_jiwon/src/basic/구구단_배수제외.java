package basic;

public class 구구단_배수제외 {
	
	public static void main(String[] args) {
		
		// 구구단 출력
		//makeGugudan(1, 9);
	
		// 구구단 3단에서 4의 배수를 제외하고 출력
		makeGugudan2(3, 4);
	}
	
	
	public static void makeGugudan(int start, int end) {
		for(int i=start; i <= end; i++) {
			for(int j=1; j<=9; j++) {
				System.out.println(i + " * " + j + " = " + (i*j));
			}
		}
	}
	
	public static void makeGugudan2(int dan, int delete) {
		int count = 0;
		for(int i=1; i<=9; i++) {
			if(!((dan*i)%4==0)) {
				System.out.println(dan + " * " + i + " = " + dan*i);
				count++;
			}
		}
		System.out.println(delete + "의 배수를 제외한 개수: " + count);
	}
	
	
}
