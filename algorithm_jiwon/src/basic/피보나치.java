package basic;

// 1 재귀
// 2 Memoization
public class 피보나치 {
	
	public static int[] table;
	
	public static void main(String[] args) {
		table = new int[7+1];
		System.out.println(fibo(7));
		System.out.println(memoizationFibo(7));
	}
	
	// 1 재귀를 이용한 피보나치
	public static int fibo(int data) {
		if(data <= 1) {
			return 1;
		}
		return fibo(data-1) + fibo(data-2);
	}
	
	// 2 Memoization을 이용한 피보나치
	public static int memoizationFibo(int data) {
		if(data <= 1) {
			table[data] = 1;
			return 1;
		}
		
		if(table[data] > 0) {
			return table[data];
		}
		
		table[data] = memoizationFibo(data-1) + memoizationFibo(data-2);
		
		return table[data];
	}
}
