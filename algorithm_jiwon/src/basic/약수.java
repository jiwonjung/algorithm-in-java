package basic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

// 입력받은 정수와, 그 수의 약수를 출력 
// 약수: 자신을 나눈 나머지 값이 0이면 약수
public class 약수 {

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine());
		
		List<Integer> list = new ArrayList<>();
		list = solve(n);
		
		for (Integer i : list) {
			System.out.print(i + " ");
		}
		
	}
	
	public static List<Integer> solve(int n) {
		
		List<Integer> list = new ArrayList<Integer>();
		
		for(int i=1; i<=n; i++) {
			if(n % i == 0) {
				list.add(i);
			}
		}
		
		return list;
	}
}
