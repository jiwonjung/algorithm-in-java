package basic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class 내림_오름차순정렬 {
	
	// 3 4 10 2 5 를 입력받으면 내림차순 출력
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int n = st.countTokens();
		
		// 배열로 담기
		int[] arr = new int[n];
		for(int i = 0; i < n; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		
		// Arrays.sort로 한번에 오름차순 정렬
//		Arrays.sort(arr);
//		for (int i : arr) {
//			System.out.print(i + " ");
//		}
		
		
		int[] result = solve(arr);
		
		for (int i : result) {
			System.out.print(i + " ");
		}
		
	}
	
	// 내림차순
	public static int[] solve(int[] arr) {
		
		// int i = 0;
		// int j = i + 1;
		
		for(int i=0; i <= arr.length-2; i++) {  // 0~3이니까 3, 4, 10, 2
			for(int j=i+1; j <= arr.length-1; j++) {  // 1~4이니까  4, 10, 2, 5
				if(arr[j] < arr[i]) {  // 부등호가 반대면 오름차순
					// swap
					int tmp = arr[j];
					arr[j] = arr[i];
					arr[i] = tmp;
				}
			}
		}
		return arr;
	}

}
