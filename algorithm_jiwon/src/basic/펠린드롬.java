package basic;

import java.io.BufferedReader;
import java.io.InputStreamReader;

// 문자열 거꾸로 뒤집기
public class 펠린드롬 {
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		
		// useFor(str);  // 1번 방식
		useArray(str);   // 2번방식
		// useStringBuffer(str);  // 3번 방식
		
	}
	
	// 1) for문을 이용한 문자열 뒤집기
	public static void useFor(String str) {
		for (int i = str.length()-1; i>=0 ; i--) {
			System.out.print(str.charAt(i));  // charAt(index)는 인덱스에 해당하는 문자열을을 반환
		}
	}
	
	// 2) for문을 이용하여 배열에 담아 옮겨담아 문자열 뒤집기
	public static void useArray(String str) {
		int length = str.length();
		
		char[] change = new char[length];
		for(int i=0; i<length; i++) {
			change[i] = str.charAt(i);  // string 문자열을 배열에 옮겨담기
		}
		
		for(int i=length-1; i>=0; i--) {
			System.out.print(change[i]);
		}
	}
	
	
	// 3) StringBuffer를 이용한 문자열 뒤집기
	public static void useStringBuffer(String str) {
		StringBuffer sb = new StringBuffer();
		sb.append(str);
		
		System.out.println(sb.reverse());
	}
}
