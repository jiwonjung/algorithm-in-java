package basic;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class 정렬_Insertion {
	// 29 10 14 37 13
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int n = st.countTokens();
		
		int[] arr = new int[n];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		
		insertionSort(arr);
	}
	
	public static void insertionSort(int[] arr) {
		//int tmp;
		int length = arr.length;
		
		for (int i = 1; i < length; i++) {
			for (int j = i; j > 0; j--) {  // 기준이 되는 아이를 왼쪽에 정렬된 애들이랑 비교해야하니 --하면서 비교해나가야함
				if(arr[j-1] > arr[j]) {
					int tmp = arr[j - 1];
					arr[j - 1] = arr[j];
					arr[j] = tmp;
				}
			}
		}
		
		// 출력
		for (int i : arr) {
			System.out.print(i + " ");
		}
	}
}
