package basic;

// 4! = 1 * 2 * 3 * 4 = 24;
//재귀를 이용한 팩터리얼
public class 팩토리얼 {
	public static void main(String[] args) {
		
		System.out.println(factorial(4));
	}
	
	public static int factorial(int data) {
		if(data <= 1) {
			return 1;
		}
		
		return factorial(data-1) * data;
	}
}
