package basic;
// 1~1000까지 3과 5의 배수의 총합계
// (ex. 1~10미만의 자연수에서 3과 5의 배수를 구하면 3,5,6,9이다. 이들의 총합은 23이다.

public class 숫자3과5의배수총합 {

	public static void main(String[] args) {
		
		method1(3, 5);
		method2(3, 5);
	}
	
	// 방법1 (간단하지만 반복문으로 1~1000까지 1000번의 반복이 발생하므로 실행시간 더 오래걸림)
	public static void method1(int num1, int num2) {
		int sum = 0;
		int range = 1000;
		
		for(int i=1; i < range; i++) { 
			if(i % num1 == 0 || i % num2 == 0) {
				sum += i; // 1부터 1000중에서 3또는 5로 나누어 나머지가 0인 값을 찾아 더해준다.
			}
		}
		System.out.println("방법1의 총합:" + sum);
	}
	
	// 방법2
	/*	1. 1~1000까지 3의 배수 합을 먼저 구한다.
		2. 1~1000까지 다시 5의 배수 합을 구한다.
   		3과 5의 최소공배수인 15는 제외하고 구해준다.
	 * */
	public static void method2(int num1, int num2) {
		int sum = 0; 
		int range = 1000;
		
		// 1~1000중 3의 배수를 더하여 sum 값 구하기
		for(int i=1; i*num1 < range; i++) {
			sum += i*num1;
		}
		
		// 1~1000중 5의 배수를더하여 sum 값 구하기
		// 단 3과 5의 최소공배수 15는 제외하고 계산
		for(int i=1; i*num2 < range; i++) {
			if((i*num2)%15 == 0) {
				continue;
			}
			sum += i*num2;
		}
		
		System.out.println("방법2의 총합:" + sum);
	}
	

}

 