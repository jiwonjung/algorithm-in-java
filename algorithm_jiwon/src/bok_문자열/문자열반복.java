package bok_문자열;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// #2675
/*
2
3 ABC
5 /HTP  ->  AAABBBCCC
			/////HHHHHTTTTTPPPPP
*/
public class 문자열반복 {
	public static void main(String[] args) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());  // 테스트 케이스 개수 
		
		int R = 0;
		String S = "";
		for(int i=0; i<T; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			R = Integer.parseInt(st.nextToken());// 반복횟수  ex) 3
			S = st.nextToken();    // 문자열 ex) ABC
			
			solve(R, S);
		}
		
	}
	
	public static void solve(int R, String str) {
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<str.length(); i++) {
			for(int j=0; j<R; j++) {
				sb.append(str.charAt(i));
			}
		}
		
		System.out.println(sb);
	}
}
