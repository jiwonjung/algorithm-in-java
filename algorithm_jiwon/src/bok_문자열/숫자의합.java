package bok_문자열;

import java.io.BufferedReader;
import java.io.InputStreamReader;

// 11720
/* 5
 * 54321   =>15
 * */
public class 숫자의합 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());  // 5
		String problem = br.readLine();  // 54321
		int sum = 0; 
		for(int i=0; i<N; i++) {
			sum += problem.charAt(i) - '0';  // char -> int위해 '0'해줌
		}
		
		System.out.println(sum);
	}
}
