package bok_문자열;

import java.io.BufferedReader;
import java.io.InputStreamReader;

// 10809   : 각각의 알파벳에 대해 위치, 존재안하면 -1 출력
// baekjoon -> 1 0 -1 -1 2 -1 -1 -1 -1 4 3 -1 -1 7 5 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1 -1
public class 알파벳찾기 {
	
	public static void main(String[] args) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();  // baekjoon
		
		findAlphabet(str);
	}
	
	public static void findAlphabet(String str) {
		// a부터 z까지 해당 문자가 등장시 해당 인덱스위치를 반환 (없으면 -1을 반환)
		for(char i='a'; i<='z'; i++) {
			System.out.print(str.indexOf(i) + " ");
		}
	}
}
