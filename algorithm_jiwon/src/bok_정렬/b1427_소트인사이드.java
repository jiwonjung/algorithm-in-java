package bok_정렬;

import java.util.Scanner;

public class b1427_소트인사이드 {
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		long[] arr = new long[str.length()];
		
		// 배열에 옮겨 담기
		for(int i=0; i<arr.length; i++) {
			arr[i] = str.charAt(i) - '0';
		}
		
		// 내림차순 정렬
		for(int i=0; i<arr.length-1; i++) {
			for(int j=i+1; j<arr.length; j++) {
				if(arr[i] < arr[j]) {
					long tmp = arr[i];
					arr[i] = arr[j];
					arr[j] = tmp;
				}
			}
		}
		
		// 출력
		for(int i=0; i<arr.length; i++) {
			System.out.print(arr[i]);
		}
		
	}
}
