package bok_정렬;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/*
 5
1 1 1 6 0
2 7 8 3 1
 * */
public class b1026_보물2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int N = scanner.nextInt();
		List<Integer> a = new ArrayList<>();
		List<Integer> b = new ArrayList<>();
		for (int i = 0; i < N; i++) {
			a.add(scanner.nextInt());
		}
		for (int i = 0; i < N; i++) {
			b.add(scanner.nextInt());
		}
		Collections.sort(a);
		Collections.sort(b, Collections.reverseOrder());
		
		
		int result = 0;
		for (int i = 0; i < N; i++) {
			result += a.get(i) * b.get(i);
		}
		System.out.println(result);
		scanner.close();
	}

}
