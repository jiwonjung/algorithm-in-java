package bok_정렬;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/*
 5
1 1 1 6 0
2 7 8 3 1
 * */
public class b1026_보물 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		
		String[] str = br.readLine().split(" ");
		int[] A = new int[str.length];
		for(int i=0; i<str.length; i++) {
			A[i] = Integer.parseInt(str[i]);
		}
		
		int[] A2 = sort(A);
		
		String[] str2 = br.readLine().split(" ");
		int[] B = new int[str2.length];
		for(int i=0; i<str2.length; i++) {
			B[i] = Integer.parseInt(str2[i]);
		}
		
		Arrays.sort(B);
		
		solve(A2, B);
	}
	
	
	public static int[] sort(int[] arr) {
		for(int i=0; i<arr.length-1; i++) {
			for(int j=i+1; j<arr.length; j++) {
				if(arr[j] > arr[i]) {  // 부등호가 반대면 오름차순
					// swap
					int tmp = arr[j];
					arr[j] = arr[i];
					arr[i] = tmp;
				}
			}
		}
		
		return arr;
	}
	
	public static void solve(int[] A, int[] B) {
		int sum = 0;
		
		
		
		for(int i=0; i<A.length; i++) {
			sum += A[i] * B[i];
			// System.out.println(sum + "=" + A[i] + "+" + B[i]);
		}
		System.out.println(sum);
	}
}
