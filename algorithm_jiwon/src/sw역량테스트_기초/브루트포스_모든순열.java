package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 순열 : n 개 중에서 r 개 선택 시간복잡도는 O(n!) 연습문제 : https://www.acmicpc.net/problem/10974
 */

public class 브루트포스_모든순열 {
	public static void main(String[] args) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		
		int[] arr = new int[N];
		
		for(int i=0; i<N; i++) {
			arr[i] = i+1;
		}
		
		do {
			for(int i=0; i<arr.length; i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println();
		} while (allPermutation(arr));
	}
	
	public static boolean allPermutation(int[] arr) {
		// 뒤에서부터 탐색하면서 a-1보다 a가 더 큰 것을 찾음
		int a = arr.length-1;
		while(a>0 && arr[a-1] >= arr[a]) {
			a--;
		}
		
		if(a <= 0 ) {
			return false;
		}
		
		// 다시 뒤에서부터 탐색하면서 a-1보다 b가 더 큰 것을 찾음
		int b = arr.length-1;
		while(arr[a-1] >= arr[b]) {
			b--;
		}
		
		// a-1와 b를 swap
		int tmp = arr[a-1];
		arr[a-1] = arr[b];
		arr[b] = tmp;
		
		// a부터 끝까지 오름차순
		b = arr.length-1;
		while(a < b) {
			tmp = arr[a];
			arr[a] = arr[b];
			arr[b] = tmp;
			a++;
			b--;
		}
		
		return true;
	}
}