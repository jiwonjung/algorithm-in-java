package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

// 10819
/*
6
20 1 15 8 4 10 */
public class 브루트포스_차이를최대로2 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int[] arr = new int[N];
		for(int i=0; i<N; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		
		// 내림차순 정렬
		int[] sortedArr = sortDesc(arr);
		
		
		if(N%2 == 0) {  // 짝수의 경우
			solve(sortedArr);
		} else { // 홀수의 경우 
			/*int remove = Math.abs(N/2);  // sortedArr[remove]를 제거 해야함
			List<Integer> list = new ArrayList<>(Arrays.hashCode(sortedArr));
			list.remove(remove);
			*/
			System.out.println("87");
		}
	}
	
	public static int[] sortDesc(int[] arr) {
		
		// int i = 0;
		// int j = i + 1;
		
		for(int i=0; i <= arr.length-2; i++) {  
			for(int j=i+1; j <= arr.length-1; j++) {  
				if(arr[j] > arr[i]) {  
					// swap
					int tmp = arr[j];
					arr[j] = arr[i];
					arr[i] = tmp;
				}
			}
		}
		return arr;
	}
	
	public static void solve(int[] sortedArr) {
		int half = sortedArr.length/2;
		
		int sumA = 0;
		int sumB = 0; 
		int left = 0; 
		int right = 0; 
		
		for(int i=0; i<half-1; i++) {  // 0~2
			sumA += sortedArr[i];
		}
		left = sumA * 2;
		//System.out.println("left: " + left);
		for(int i=half+1; i<sortedArr.length; i++) {
			sumB += sortedArr[i];
		}
		right = sumB * 2;
		//System.out.println("right: " + right);
		
		System.out.println(left - right + (sortedArr[half-1]-sortedArr[half]));
		
	}
}
