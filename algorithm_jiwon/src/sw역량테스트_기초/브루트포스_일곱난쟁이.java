package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class 브루트포스_일곱난쟁이 {
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int max = 100;
		int sum = 0;
		
		int[] dwarfs = new int[9];
		
		for(int i=0; i<dwarfs.length; i++) {
			dwarfs[i] = Integer.parseInt(br.readLine());
			sum += dwarfs[i];
		}
		
		boolean checked = false; 
		for (int i = 0; i < dwarfs.length-1; i++) {
			for (int j = i+1; j < dwarfs.length; j++) {
				// 풀이: 두 난쟁이의 키를 전체 아홉 난쟁이 키의 총합에서 뺀 값이 100이면 두 난쟁이의 값을 최소 값으로 변경
				if(sum-(dwarfs[i]+dwarfs[j]) == max) {  // 빼야할 난쟁이들을 찾을 시 
					dwarfs[i] = Integer.MIN_VALUE;
					dwarfs[j] = Integer.MIN_VALUE;
					checked = true;
					break;
				}
				
				if(checked) {
					break;
				}
			}
		}
		
		// 오름차순 정렬하고 출력
		Arrays.sort(dwarfs);
		
		// 출력
		for (int i : dwarfs) {
			if(i != Integer.MIN_VALUE) {
				System.out.println(i);
			}
		}
	}
}
