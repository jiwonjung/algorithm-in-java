package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 1934
/*
3
1 45000
6 10
13 17*/
public class 수학_최소공배수 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());
		
		for(int i=0; i<T; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int A = Integer.parseInt(st.nextToken());	
			int B = Integer.parseInt(st.nextToken());
			
			int GCD = getGCD(Math.max(A, B), Math.min(A, B));
			int LCM = getLCM(A, B, GCD);
			
			System.out.println(LCM);
		}
	}
	
	public static int getGCD(int A, int B) {
		while(B != 0) {
			int tmp = A;
			A = B;
			B = tmp%B;
		}
		return A;
	}
	
	public static int getLCM(int A, int B, int GCD) {
		
		return A * B / GCD;
	}
}
