package sw역량테스트_기초;


import java.io.BufferedReader;
import java.io.InputStreamReader;

class 브루트포스_로또 {
   //백트레킹 dfs로 풀기   (시간: 168m)
	
	static int N;
	static int arr[];
	static int result[];
	
	public static void main(String[] args) throws Exception {
		//input 
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		while(true) {
			String str[] = br.readLine().split(" ");
			N = Integer.parseInt(str[0]);
			arr = new int[N];
			result = new int[N];
			
			if (N == 0) break;
			
			for (int i = 0; i < N; i++) {
				arr[i] = Integer.parseInt(str[i + 1]);
			}
			
			DFS(0, 0);
			System.out.println();
		}
	
	}

	private static void DFS(int start, int depth) {
		if (depth == 6) {
			//출력
			for(int i = 0; i < N; i++) {
				if (result[i] == 1) {
					System.out.print(arr[i] + " ");
				}
			}
			System.out.println();
		}
		
		for (int i = start; i < N; i++) {
			result[i] = 1;
			DFS(i+1, depth+1);
			result[i] = 0;
		}
	}
}