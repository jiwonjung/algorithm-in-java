package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;

// 9095
/*
3
4
7
10
 * */
public class 브루트포스_123더하기 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int T = Integer.parseInt(br.readLine());
		
		int[] dp = new int[11];  // 각 n은 양수이며 11보다 작은 수가 들어오는 문제이므로 11로 사이즈를 만들어줌
		
		dp[0] = 0;
		dp[1] = 1;
		dp[2] = 2;
		dp[3] = 4;
		
		for(int i=0; i<T; i++) {
			int n = Integer.parseInt(br.readLine());
			
			for(int j=4; j<=n; j++) {
				dp[j] = dp[j-1] + dp[j-2] + dp[j-3];
			}
			
			System.out.println(dp[n]);
		}
		
	}
}
