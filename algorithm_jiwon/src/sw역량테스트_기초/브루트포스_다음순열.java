package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 10972
/*
4
1 2 3 4
 * */
public class 브루트포스_다음순열 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		int[] arr = new int[N];
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		for (int i = 0; i < arr.length; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		
		if(nextPermutation(arr)) {
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i] + " ");  // 다음순열 한 번만 출력
			}
		} else {
			System.out.println("-1");
		}
	}
	
	public static boolean nextPermutation(int[] arr) {
		// 뒤에서부터 탐색하면서 a-1보다 a가 더 큰 것을 찾음
		int a = arr.length - 1;
		
		while(a > 0 && arr[a-1] >= arr[a]) {
			a--;
		}
		if (a <= 0 ){
			return false; // arr의 길이가 1이라면, 결국 사전순으로  마지막에 오는 순열도 되므로 false 반환
		}
		
		// 다시 뒤에서부터 탐색하면서 a-1보다 b가 더 큰 것을 찾음
		int b = arr.length-1;
		while(arr[a-1]>= arr[b]) {
			b--;
		}
		
		// a-1와 b를 swap
		int tmp = arr[a-1];
		arr[a-1] = arr[b];
		arr[b] = tmp; 
		
		// a부터 끝까지 오름차순
		int end = arr.length-1;
		
		while(a < end) {
			tmp = arr[a];
			arr[a] = arr[end];
			arr[end] = tmp;
			a++;
			end--;
		}
		
		return true;
	}
	
}
