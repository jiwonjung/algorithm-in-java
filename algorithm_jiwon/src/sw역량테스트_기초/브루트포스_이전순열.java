package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 10973
/*
5
5 4 3 2 1*/
public class 브루트포스_이전순열 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		
		int[] arr = new int[N];
		StringTokenizer st = new StringTokenizer(br.readLine());
		for(int i=0; i<N; i++) {
			arr[i] = Integer.parseInt(st.nextToken()); // 배열에 넣기
		}
		
		if(prevPermutation(arr)) {
			for(int i=0; i<arr.length; i++) {
				System.out.print(arr[i] + " ");  
			}
		} else {
			System.out.println("-1");
		}
	}
	
	public static boolean prevPermutation(int[] arr) {
		// 뒤에서부터 탐색하며 a-1가 a보다 큰 것을 찾아 그 사이를 두 영역으로 나눔
		int a = arr.length-1;
		while(a>0 && arr[a-1] <= arr[a]) {
			a--;
		}
		if(a <= 0) return false;
		
		// 다시 뒤에서부터 탐색하며 a-1가 b보다 큰 것을 찾음
		int b = arr.length-1;
		while(arr[a-1] <= arr[b]) {
			b--;
		}
		
		// a-1와 b를 swap
		int tmp = arr[a-1];
		arr[a-1] = arr[b];
		arr[b] = tmp;
		
		// a부터 끝까지를 내림차순 정렬 
		int end = arr.length-1;
		while(a < end) {
			tmp = arr[a];
			arr[a] = arr[end];
			arr[end] = tmp;
			a++;
			end--;
		}
		
		return true;
	}
}
