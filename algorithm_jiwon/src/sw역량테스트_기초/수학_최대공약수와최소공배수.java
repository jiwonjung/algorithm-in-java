package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 2609
/*
최대공약수(GCD): 공통된 약수 중 가장 큰 수   (0으로 나눌 수 있는 수 ex) 12의 약수: 1, 2, 3, 4, 6, 12) 
최소공배수(LCM): 공통된 배수 중 가장 작은 수 (4의 배수란.. ex) 4, 8, 12, 16, 20,,,)
* */
public class 수학_최대공약수와최소공배수 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int A = Integer.parseInt(st.nextToken());
		int B = Integer.parseInt(st.nextToken());
		
		int GCD = getGCD(Math.max(A, B), Math.min(A, B));  // 큰 수를 앞에, 작은 수를 뒤에
		int LCM = getLCM(A, B, GCD);
		
		System.out.println(GCD);
		System.out.println(LCM);
	}
	
	public static int getGCD(int A, int B) {
		while(B != 0) {
			int tmp = A;
			A = B;
			B = tmp%B;
		}
		return A;
	}
	
	public static int getLCM(int A, int B, int GCD) {
		return A * B / GCD;
	}
	
}
