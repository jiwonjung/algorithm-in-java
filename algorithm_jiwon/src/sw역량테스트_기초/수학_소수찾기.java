package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 1978
/*
4
1 3 5 7
 * */
public class 수학_소수찾기 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());  // 4
		
		
		int cnt = 0; 
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		int[] arr = new int[N];
		
		for(int y=0; y<N; y++) {
			arr[y] = Integer.parseInt(st.nextToken());  // 배열에  넣기 
			boolean result = isPrime(arr[y]);
			if(result == true) {
				cnt++;
			}
		}
		
		System.out.println(cnt);
	}
	
	public static boolean isPrime(int num) {
		if(num == 1) {
			return false;
		} else {
			for(int i=2; i<num; i++) {
				if(num%i == 0) {  // 하나라도 나누어지는 것이 존재하면 소수가 아니므로 false 반환 
					return false;
				}
			}
		}
		
		return true;  // 소수의 경우, true 반환
	}
}
