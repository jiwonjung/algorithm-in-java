package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 9613
/*
3
4 10 20 30 40
3 7 5 12
3 125 15 25
 * */
public class 수학_GCD합 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int testCase = Integer.parseInt(br.readLine());
		
		for(int i=0; i<testCase; i++) {
			StringTokenizer st = new StringTokenizer(br.readLine());
			int N = Integer.parseInt(st.nextToken());
			int[] arr = new int[N];
			
			for(int x=0; x<N; x++) {
				arr[x] = Integer.parseInt(st.nextToken());  // 배열에 담기
			}
			
			long answer = 0;
			for(int k=0; k<arr.length-1; k++) {
				for(int j=k+1; j<arr.length; j++) {
					int GCD = getGCD(arr[k], arr[j]);
					answer += GCD;
				}
			}
			System.out.println(answer);
		}
	}
	
	public static int getGCD(int A, int B) {
		int a = Math.max(A, B);
		int b = Math.min(A, B);
		
		while(b != 0) {
			int tmp = a;
			a = b;
			b = tmp%b;
		}
		
		return a;
	}
}
