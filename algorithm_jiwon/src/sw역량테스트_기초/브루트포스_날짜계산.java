package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class 브루트포스_날짜계산 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		int E = Integer.parseInt(st.nextToken());
		int S = Integer.parseInt(st.nextToken());
		int M = Integer.parseInt(st.nextToken());
		
		// 변하는 각 변수 초기화
		int tE = 1;
		int tS = 1;
		int tM = 1;
		
		int result = 0;
		
		while(true) {
			result++; // 년도 증가
			// 입력값과 같아진 다면 년도 출력 후 종료
			if(E == tE && S == tS && M == tM) {
				System.out.println(result);
				break;
			}
			
			tE++;
			tS++;
			tM++;
			
			// 각 범위를 넘어서면 1로 초기화
			if(tE > 15) tE =1;
			if(tS > 28) tS =1;
			if(tM > 19) tM =1;
			
		}
	}
}
