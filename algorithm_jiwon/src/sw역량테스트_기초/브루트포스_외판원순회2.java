package sw역량테스트_기초;

import java.util.Scanner;

/*
4
0 10 15 20
5  0  9 10
6 13  0 12
8  8  9  0
 * */
//https://harj.tistory.com/50
public class 브루트포스_외판원순회2 {
  public static void main(String[] args){
  	//input
  	Scanner sc = new Scanner(System.in);
  	int N = sc.nextInt();
  	
  	int[][] money = new int[N][N]; //도시a에서 도시b로 가기 위한 비용
  	int [] city = new int[N];  //어떤 도시를 방문할지를 city[]라는 배열에 넣음
  	
  	for(int i = 0; i < N; i++) {
  		for(int j = 0; j < N; j++) {
  			money[i][j] = sc.nextInt();
  		}
  	}
  	
  	for(int i = 0; i < N; i++) {
  		city[i] = i;  //city[0], city[1]...city[n-1]은 도시1, 도시2, ...도시n-1 나타냄
  	}
  	
  	int answer = Integer.MAX_VALUE; 
  	
  	do{
          int i = 0;
          int sum = 0; //  해당 루프가 진행되는 동안 가장 작은 값(가장 작은 순회비용)
          boolean flag = true;

          while(i < N){

              // i가 마지막원소일 때
              // 다시 처음 동네로 돌아가야함으로 b[0]로 접근!
              if(i == N - 1){
                  
                  // 다음 장소로 이동하면서 이동하지 못할 경우에 0으로 표현되므로 0일경우에는 flag를 true로
                  // 최소값 비교조차 하지 않도록 함
                  if(money[city[i]][city[0]] == 0){
                      flag = false;
                      break;
                  }
                  sum += money[city[i]][city[0]];
              }else {

                  // 다음 장소로 이동하면서 이동하지 못할 경우에 0으로 표현되므로 0일경우에는 flag를 true로
                  // 최소값 비교조차 하지 않도록 함
                  if(money[city[i]][city[i+1]] == 0){
                      flag = false;
                      break;
                  }
                  sum += money[city[i]][city[i+1]];
              }
              i++;
          }
          if (flag && answer > sum){
          	answer = sum; //최소비용으로 answer에 넣음 
          }

      }while(next_permutation(city));

      System.out.println(answer);
  }

	public static boolean next_permutation(int[] arr) {
		//뒤에서부터 탐색하면서 a-1보다 a가 더 큰 경우 찾음
				int a = arr.length - 1;
				while(a > 0 && arr[a-1] >= arr[a]) a--;
				if (a <= 0 ) return false;
				
				//다시 뒤에서부터 탐색하며 a-1보다 b가 더 큰 경우 찾음
				int b = arr.length - 1;
				while(arr[a-1] >= arr[b]) b--;
				
				//a랑 b를 swap
				int tmp = arr[a-1];
				arr[a-1] = arr[b];
				arr[b] = tmp;
				
				//a에서부터 끝까지를 오름차순 정렬 
				int start = a;
				int end = arr.length - 1;
				while(start < end) {
					tmp = arr[start];
					arr[start] = arr[end];
					arr[end] = tmp;
					start++;
					end--;
				}
				return true;	
	}
}