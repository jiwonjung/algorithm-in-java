package sw역량테스트_기초;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

//10819
/*
6
20 1 15 8 4 10 */
public class 브루트포스_차이를최대를 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		int N = Integer.parseInt(br.readLine());
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int[] arr = new int[N];
		for(int i=0; i<N; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		
		Arrays.sort(arr);
		
		int answer = 0 ;
		
		do {
			int calculate_result = sum(arr);
			answer = Math.max(answer, calculate_result);
			
			
		} while (nextPermutation(arr));
		
		System.out.println(answer);
	}	
	
	public static int sum(int[] arr) {
		int sum = 0; 
		for(int i=0; i<arr.length; i++) {
			sum += arr[i]+arr[i+1];
		}
		
		return sum;
	}
	
	public static boolean nextPermutation(int[] arr) {
		// 
		int a = arr.length-1;
		while(a>=0 && arr[a-1] >= arr[a]) {
			a--;
		}
		if(a <= 0) return false;
		
		// 
		int b = arr.length-1;
		while(arr[a-1] >= arr[b]) {
			b--;
		}
		
		// swap
		int tmp = arr[a-1];
		arr[a-1] = arr[b];
		arr[b] = tmp;
		
		//
		int end = arr.length-1;
		while(a < end) {
			tmp = arr[a];
			arr[a] = arr[end];
			arr[end] = tmp;
			a++;
			end--;
		}
		return true;
	}
}
