package codeup_basic100;

import java.util.Scanner;

// 정수 2개 입력받아 합 출력하기2
public class n1039 {
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		
		long a = sc.nextLong();
		long b = sc.nextLong();
		System.out.println(a + b);
	}
}
