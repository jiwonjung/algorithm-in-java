package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class c1023 {
	public static void main(String[] args) throws Exception {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String str = br.readLine();
		StringTokenizer st = new StringTokenizer(str, ".");
		
		int[] data = new int[2];
		
		int i = 0;
		while(st.hasMoreTokens()) {
			data[i] = Integer.parseInt(st.nextToken());
			i++;
		}
		
		System.out.println(data[0]);
		System.out.println(data[1]);
		
	}
}
