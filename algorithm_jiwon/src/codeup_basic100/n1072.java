package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.StringTokenizer;

// 정수 입력받아 계속 출력하기
public class n1072 {
	public static void main(String[] args) throws Exception {
		
		Scanner sc = new Scanner(System.in);
		long l = sc.nextLong();
		StringTokenizer st = new StringTokenizer(sc.nextLine());
		
		while(st.hasMoreTokens()) {
			System.out.println(st.nextToken() + " ");
		}
	}
}
