package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 년월일 입력 받아 형식 바꿔 출력하기
// 2014.07.15 ==> 15-07-2014 
public class n1027 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine(), ".");
		
		String year = st.nextToken();
		String month = st.nextToken();
		String date = st.nextToken();
		
		System.out.println(date + "-" + month + "-" + year);
	}
}
