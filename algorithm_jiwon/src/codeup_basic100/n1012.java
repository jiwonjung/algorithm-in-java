package codeup_basic100;
import java.util.Scanner;

public class n1012 {
	public static void main(String[] args) throws Exception {
		
		Scanner sc = new Scanner(System.in);
		float f = sc.nextFloat();
		String answer = String.format("%.6f", f);
		System.out.println(answer);
	}
}
