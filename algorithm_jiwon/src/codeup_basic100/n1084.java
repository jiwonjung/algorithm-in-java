package codeup_basic100;

import java.util.Scanner;

// 빛 섞어 색 만들기
public class n1084 {
	public static void main(String[] args) throws Exception {
	/*	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int r = Integer.parseInt(st.nextToken());
		int g = Integer.parseInt(st.nextToken());
		int b = Integer.parseInt(st.nextToken());
		*/
		
		Scanner sc = new Scanner(System.in);
		int r = sc.nextInt();
		int g = sc.nextInt();
		int b = sc.nextInt();
		
		int i, j, k, cnt = 0;
		
		for(i=0; i<r; i++) {
			for(j=0; j<g; j++) {
				for(k=0; k<b; k++) {
					System.out.printf("%d %d %d\n",i, j, k);
					cnt++;
				}
			}
		}
		
		System.out.println(cnt);
	}
}
