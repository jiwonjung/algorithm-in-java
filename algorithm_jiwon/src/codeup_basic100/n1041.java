package codeup_basic100;

import java.util.Scanner;

// 문자 1개 입력받아 다음 문자 출력하기
public class n1041 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String i = sc.nextLine();
		int result = i.charAt(0) + 1;
		System.out.println((char)result);
	}
}
