package codeup_basic100;

import java.util.Scanner;

// 정수 1개 입력받아 부호 바꿔 출력하기
public class n1040 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		long N = sc.nextLong();
		
		System.out.println(N*(-1));
	}
}
