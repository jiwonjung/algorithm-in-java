package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

// 3 6 9 게임의 왕이 되자!
public class n1083 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int a = Integer.parseInt(st.nextToken());
		
		StringBuilder sb = new StringBuilder();
		
		for(int i=1; i<=a; i++) {
			if(i % 3 == 0) {
				sb.append("X ");
			} else {
				sb.append(i + " ");
			}
			
		}
		
		System.out.println(sb);
	}
}
