package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 언제까지 더해야 할까?
public class n1080 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int a = Integer.parseInt(st.nextToken());
		
		int index = 1;
		int sum = 0;
		for(int i=0; i<=1000; i++) {
			sum += index;
			if(sum == a || sum > a) {
				System.out.println(index);
				break;
			}
			index++;
		}
		
	}
}
