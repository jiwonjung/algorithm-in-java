package codeup_basic100;

import java.util.Scanner;

// 정수 1개 입력받아 1 더해 출력하기
public class n1044 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		long a = sc.nextLong();
		System.out.println(a+1);
	}
}
