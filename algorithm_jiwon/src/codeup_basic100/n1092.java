package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 함께 문제 푸는 날
public class n1092 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int a = Integer.parseInt(st.nextToken());
		int b = Integer.parseInt(st.nextToken());
		int c = Integer.parseInt(st.nextToken());
		
		int pa, pb, pc = 0;
		
		int index = 1;
	
		while(true) {
			pa = a * index; // 3*1 3*2 3*3	
			pb = b * index; // 7*1 7*2 7*3	
			pc = c * index; // 9*1 9*2 9*3	
			
			index++;
			System.out.println("pa: " + pa);
			System.out.println("pb: " + pb);
			System.out.println("pc: " + pc);
			System.out.println();
			// 3  6   9   12   15  18   24   27   30   33  
			// 7 14  21   28   35  42
			// 9 18  27   36   45  54
			if(pa == pb && pb == pc) {
				System.out.println(pa);
				break;
			}
		}
		
	}
}
