package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 정수 2개 입력받아 합 출력하기1
public class n1038 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		long l1 = Integer.parseInt(st.nextToken());
		long l2 = Integer.parseInt(st.nextToken());
		
		System.out.println(l1 + l2);
	}
}
