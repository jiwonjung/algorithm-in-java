package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class n1010 {
	
	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		int N = Integer.parseInt(br.readLine());
		
		System.out.println(N);
	}
}
