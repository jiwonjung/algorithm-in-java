package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;

// 정수 1개 입력받아 나누어 출력하기
public class c1025 {	
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		//int N = Integer.parseInt(br.readLine());
		String line = br.readLine();
		int[] arr = new int[5];
		
		for(int i=0; i<arr.length; i++) {
			arr[i] = line.charAt(i) - '0';  // line.charAt(i) - '0'는 char에서 int로 바꾸는 방법
			
		}
		System.out.printf("[%d]\n", arr[0]*10000);
		System.out.printf("[%d]\n", arr[1]*1000);
		System.out.printf("[%d]\n", arr[2]*100);
		System.out.printf("[%d]\n", arr[3]*10);
		System.out.printf("[%d]", arr[4]);
		
	}
}
