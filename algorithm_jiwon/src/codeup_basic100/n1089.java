package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 수 나열하기1
/* 1 3 5 ==>13
 */
public class n1089 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int a = Integer.parseInt(st.nextToken()); // 시작 값(a)
		int d = Integer.parseInt(st.nextToken()); // 등차의 값(d)
		int n = Integer.parseInt(st.nextToken()); // 몇 번째 수 인지를 의미하는 정수(n)
		
		for(int i=0; i<n-1; i++) {
			a += d;
		}
		
		System.out.println(a);
	}
}
