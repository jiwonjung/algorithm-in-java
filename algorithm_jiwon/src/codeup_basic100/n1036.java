package codeup_basic100;

import java.util.Scanner;

// 영문자 1개 입력받아 10진수로 출력하기
public class n1036 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		int result = str.charAt(0);
		System.out.println(result);
	}
}
