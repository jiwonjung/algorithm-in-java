package codeup_basic100;

import java.util.Scanner;

// 참 거짓 바꾸기
public class n1053 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		System.out.println((n == 0) ? 1: 0);
	}
}
