package codeup_basic100;

import java.util.Scanner;

// 정수 입력받아 아스키 문자로 출력하기
public class n1037 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = sc.nextInt();
		
		System.out.println((char)N);
	}
}
