package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 짝수 합 구하기
public class n1079 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int a = Integer.parseInt(st.nextToken());
		
		int sum = 0;
		for(int i=1; i<=a; i++) {
			if(i % 2 == 0) {
				sum += i;
			}
		}
		
		System.out.println(sum);
	}
}
