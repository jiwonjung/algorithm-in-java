package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 정수 3개 입력받아 짝/홀 출력하기
public class n1066 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int a = Integer.parseInt(st.nextToken());
		int b = Integer.parseInt(st.nextToken());
		int c = Integer.parseInt(st.nextToken());
		
		if(a % 2 == 0) {
			System.out.println("even");
		} else System.out.println("odd");
		
		if(b % 2 == 0) {
			System.out.println("even");
		} else System.out.println("odd");
		
		if(c % 2 == 0) {
			System.out.println("even");
		} else System.out.println("odd");
		
	}
}
