package codeup_basic100;

import java.util.Scanner;

// 10진 정수 1개 입력받아 8진수로 출력하기
public class n1031 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		System.out.printf("%o", n);
	}
}
