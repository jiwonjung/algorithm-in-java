package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 3의 배수는 통과?
public class n1088 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int a = Integer.parseInt(st.nextToken());
		
		for(int i=1; i<=a; i++) {
			if(i % 3 == 0) {
				continue;
			} else {
				System.out.print(i + " ");
			}
		}
	}
}
