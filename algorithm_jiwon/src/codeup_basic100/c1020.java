package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 주민번호 입력받아 형태 바꿔 출력하기
// 000907-1121112 ==> 0009071121112
public class c1020 {

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String str = br.readLine();
		StringTokenizer st = new StringTokenizer(str, "-");
		
		StringBuilder sb = new StringBuilder();  // StringBuilder가 String보다 문자열 추가시 새로운 메모리 생성안하므로	
		
		while(st.hasMoreTokens()) {
			sb.append(st.nextToken());
			
		}
		System.out.println(sb);
	
	}

}
