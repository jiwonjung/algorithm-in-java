package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class n1034 {
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String n = br.readLine();  // 8진수로 받았고..
		
		int ans = Integer.valueOf(n, 8);  // 8진수->10진수로 변환
		
		System.out.println(ans);
	}
}	
