package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 정수 1개 입력받아 분석하기
public class n1067 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int a = Integer.parseInt(st.nextToken());
		
		if(a < 0) {
			System.out.println("minus");
		} else {
			System.out.println("plus");
		}
		
		if(a % 2 == 0) {
			System.out.println("even");
		} else {
			System.out.println("odd");
		}
	}
}
