package codeup_basic100;

import java.util.Scanner;

// 정수 1개 입력받아 2배 곱해 출력하기
public class c1047 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		long n = sc.nextLong();
		System.out.println(n*2);
	}
	
}
