package codeup_basic100;

import java.util.Scanner;

// 10진 정수 입력받아 16진수로 출력하기1
public class n1032 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		System.out.printf("%x", n);
	}
}
