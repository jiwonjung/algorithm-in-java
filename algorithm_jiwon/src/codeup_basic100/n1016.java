package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;

// 정수 1개 입력받아 3번 출력하기
public class n1016 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int N = Integer.parseInt(br.readLine());
		
		for(int i=0; i<3; i++) {
			System.out.print(N + " ");
		}
	}
	
}
