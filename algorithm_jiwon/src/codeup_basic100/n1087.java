package codeup_basic100;

import java.util.Scanner;

// 여기까지! 이제 그만~
public class n1087 {
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		long n = sc.nextLong();
		long sum = 0;
		for(long i=1; i<=100000000; i++) {
			sum += i;
			if(sum >= n) {
				System.out.println(sum);
				break;
			}
		}
	}
}
