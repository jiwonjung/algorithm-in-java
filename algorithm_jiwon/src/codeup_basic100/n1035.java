package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

// 16진 정수 1개 입력받아 8진수로 출력하기
public class n1035 {
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String n = br.readLine();  // 16진수로 받았다고 치고...
		
		int ans = Integer.valueOf(n, 16);  // 16진수->10진수로 변환
		String result = Integer.toOctalString(ans); // 10진수 -> 8진수
		System.out.println(result);
	}
}
