package codeup_basic100;

import java.util.Scanner;

// 수 나열하기2
public class n1090 {
	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		
		long a = sc.nextLong(); // 시작 값(a)
		long d = sc.nextLong(); // 등차의 값(d)
		long n = sc.nextLong(); // 몇 번째 수 인지를 의미하는 정수(n)
		
		for(int i=1; i<n; i++) {
			a *= d;
		}
		
		System.out.println(a);
	}
}
