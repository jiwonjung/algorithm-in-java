package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 연월일 입력받아 그대로 출력하기
public class c1018 {

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String date = br.readLine();
		StringTokenizer st = new StringTokenizer(date, ".");
		
		int[] dates = new int[3];
		
		int i = 0;
		while(st.hasMoreTokens()) {
			dates[i] = Integer.parseInt(st.nextToken());
			i++;
		}
		
		String format1 = String.format("%04d", dates[0]);
		String format2 = String.format("%02d", dates[1]);
		String format3 = String.format("%02d", dates[2]);
		
		System.out.println(format1 + "." + format2 + "." + format3);
		
	}

}
