package codeup_basic100;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// 정수 1개 입력받아 평가 출력하기
/* 90 ~ 100 : A
 70 ~   89 : B
 40 ~   69 : C
   0 ~   39 : D*/
public class n1073 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int a = Integer.parseInt(st.nextToken());
		
		if(a >= 90 && a <= 100) {
			System.out.println("A");
		} else if (a >= 70 && a <= 89) {
			System.out.println("B");
		} else if(a >= 40 && a <= 69) {
			System.out.println("C");
		} else {
			System.out.println("D");
		}
	}
}
