package bok_greedy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;
 
public class b1931_회의실배정 {
 
    public static void main(String[] args) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int N = Integer.parseInt(br.readLine());
        Interval[] iv = new Interval[N];
        
        for(int i=0; i<N; i++) {
            StringTokenizer st = new StringTokenizer(br.readLine());
            int b = Integer.parseInt(st.nextToken());
            int c = Integer.parseInt(st.nextToken());
            
            iv[i] = new Interval(b,c);
        }
        
        Arrays.sort(iv);
                
        int count = 0;
        int end = -1;
        
        for(int i=0; i<N; i++) {
            if(iv[i].start >= end) {
                end = iv[i].end;
                count++;
            }
        }
        
        System.out.println(count);
        
    }
 
}
//iv 클래스를 새로 만든다.
class Interval implements Comparable<Interval>{
    int start;
    int end;
    
    public Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }
 
    @Override
    public int compareTo(Interval o) {
        if(this.end == o.end) {
            return Integer.compare(this.start, o.end);
        } else {
            return Integer.compare(this.end, o.end);
        }
    }
}