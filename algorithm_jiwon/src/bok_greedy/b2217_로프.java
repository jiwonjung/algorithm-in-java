package bok_greedy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
 
public class b2217_로프 {
 
    public static void main(String args[]) throws NumberFormatException, IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine()); // 로프의 개수
        int arr[]= new int[n];
        for(int i = 0 ; i < n ; i++){
            arr[i]=Integer.parseInt(br.readLine());
        }
        
        
        // 각 로프가 버틸 수 있는 최대 중량을 오름차순 정렬
        Arrays.sort(arr);
        
        int max = 0;
        for(int i=0 ; i<n ; i++){
            /*if(arr[i]*(n-i) > max){
                max = arr[i]*(n-i);    // 방법1
            }*/
            
            max = Math.max(max, arr[i]*(n-i));   // 방법2
        } 
        System.out.println(max);
    }
}

