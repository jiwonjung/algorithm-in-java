package bok_greedy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

// adaabc aababbc
public class b1120_문자열 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());
		String A = st.nextToken();
		String B = st.nextToken();
		
		int ans = A.length();
		
		for(int i=0; i<=B.length()-A.length(); i++) {  // i=0, i=1까지 
			int cnt = 0;
			for(int j=0; j<A.length(); j++) {
				if(A.charAt(j) != B.charAt(i+j)) {
					cnt++;
				}
			}
			ans = Math.min(ans, cnt);
		}
		
		System.out.println(ans);
	}
}
