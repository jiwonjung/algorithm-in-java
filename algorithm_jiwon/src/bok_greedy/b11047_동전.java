package bok_greedy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
/*
10 4790
1
5
10
50
100
500
1000
5000
10000
50000*/
public class b11047_동전 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st = new StringTokenizer(br.readLine());  // 10 4790
		
		int N = Integer.parseInt(st.nextToken()); // 동전의 개수
		int money = Integer.parseInt(st.nextToken()); // 돈액수 4790
		
		int[] arr = new int[N];
		
		// 두번째 줄부터 N개 만큼 동전의 종류 입력받기
		for(int i=0; i<N; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		solve(money, arr);
	}
	
	public static void solve(int money, int[] arr) {
		int cnt = 0;
		
			for(int i=arr.length-1; i>=0; i--) {
				if(arr[i] <= money) {
					cnt += money / arr[i];
					money = money%arr[i];
				}
			}
		
		System.out.println(cnt);
	}
}
