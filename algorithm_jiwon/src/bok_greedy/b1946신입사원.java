package bok_greedy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class b1946신입사원 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int testCase = Integer.parseInt(br.readLine());
		
		
		while(testCase-- > 0) {
			int N = Integer.parseInt(br.readLine());
			int[] arr = new int[N+1];
			
			for(int i=0; i<N; i++) {
				StringTokenizer st = new StringTokenizer(br.readLine());
				int X = Integer.parseInt(st.nextToken());
				int Y = Integer.parseInt(st.nextToken());
				
				arr[X] = Y;
			}
			
			int cnt = 1; // x가 1일때는 무조건 가능하므로 1로 시작
			int standard = arr[1]; // 기준 값, 처음에는 x가 1일 때의 y값
			for(int i=2; i<=N; i++) {
				if(standard > arr[i]) { // 기준 값보다 a[i]의 y값이 작다면 
					cnt++;
					standard = arr[i];  // 기준 값 a[i]의 y값으로 변경
				}
			}
			
			System.out.println(cnt);
		}
	}
}
