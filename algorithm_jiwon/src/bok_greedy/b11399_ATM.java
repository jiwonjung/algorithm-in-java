package bok_greedy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class b11399_ATM {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
		int N = Integer.parseInt(br.readLine());
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int[] arr = new int[N+1];
		
		// 그릇에 담기
		for(int i=0; i<N; i++) {
			arr[i] = Integer.parseInt(st.nextToken());
		}
		
		// 정렬하기 
		Arrays.sort(arr);
		
		solve(arr);
	}
	
	public static void solve(int[] arr) {  
		// arr: 1 2 3 3 4
		int sum = 0;
		int tmp = 0;
		for(int i=0; i<arr.length; i++) {
			tmp += arr[i];
			sum += tmp;
		}
		
		System.out.println(sum);
	}
}
