package bok_greedy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/*문제는 String배열과 split으로 문자열을 나누어 문제를 해결하였습니다. 
 * 문제를 단순하게 들여다 보면 쉽게 해결할 수 있는 단순 수학 문제입니다.
 *  +와- 기호만 있는 연산에서 결괏값을 가장 작게 만들기 위한 방법은 -기호를 기준으로 괄호를 만드는 것입니다. 
 *  즉 모든 -기호를 기준으로 괄호를 만들어 다음 -기호를 만나기 전까지 모든 +기호를 -연산으로 바꾸는 것입니다. 
 *  아래 예시를 보면 쉽게 이해할 수 있습니다.
 *  55-50+40-30+40+30-20   =>  55 - (50+40) - (30+40+30) - 20   => 결과값은 -155 */
public class b1541_잃어버린괄호 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String[] input = br.readLine().split("\\-");   // input[0]=55,  input[1]=50+40,  input[2]=30+40+30,  input[3]=20  

		int minResult = 0;

		for (int i = 0; i < input.length; i++) {
			int calcNum = calc(input[i]);  // 55, 90, 100, 20이 각각 calNum에 들어올 것임

			if (i == 0) {
				calcNum *= -1;  // 첫 숫자는 무조건 +연산자의 정수이므로, -1  곱하여 정수로 만들어줌
			}
			minResult -= calcNum; // 0-(-55) - 90 - 100 - 20
		}

		System.out.println(minResult);
	}
	
	// 주어진 문자열에서 숫자만 추출하여 모두 더해주는 연산을 담당
	private static int calc(String str) {
	    String[] subNums = str.split("\\+");  
	    int result = 0;

	    for (String item : subNums) {
	        result += Integer.parseInt(item);  
	    }
	    return result;
	}
}
