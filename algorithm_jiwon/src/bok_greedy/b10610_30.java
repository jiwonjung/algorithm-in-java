package bok_greedy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class b10610_30 {
	
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		String str = br.readLine();
		
		List<Integer> list = new ArrayList<>();
		
		int sum = 0; 
		for(int i=0; i<str.length(); i++) {
			list.add(str.charAt(i) - '0');  // 리스트에 넣기
			sum +=(str.charAt(i) - '0');  // 3의 배수의 특징으로 모든 자리수에 있는 수를 합한게 3의  배수여야 함
		}
		
		Collections.sort(list);  // 오름차순 정렬
		if(list.get(0)==0 && sum%3==0) {  // 30의 배수가 맞다면
			for(int i=list.size()-1; i>=0; i--) {
				System.out.print(list.get(i));
			}
		} else {
			System.out.println("-1");
		}
	}
}
