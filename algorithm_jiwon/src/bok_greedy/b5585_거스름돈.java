package bok_greedy;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class b5585_거스름돈 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	
		int N = Integer.parseInt(br.readLine()); // 지불할 금액  ex) 380
		
		int money = 1000 - N; // 거슬러 받는 금액 총액수  ex) 1000-380 = 620
		
		int[] type = {500, 100, 50, 10, 5, 1};
		int cnt = 0;
		
			for(int i=0; i<type.length; i++) {
				if(money >= type[i]) {
					cnt += money / type[i];
					money %= type[i];
				}
			}
			System.out.println(cnt);
	}
}
