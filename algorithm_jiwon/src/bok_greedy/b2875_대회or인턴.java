package bok_greedy;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class b2875_대회or인턴 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		StringTokenizer st = new StringTokenizer(br.readLine());
		
		int girls = Integer.parseInt(st.nextToken());  // 여학생
		int boys = Integer.parseInt(st.nextToken());  // 남학생
		int K = Integer.parseInt(st.nextToken());     // 인턴쉽에 참여해야 하는 학생
		
		int teams = 0;
		
		// 여학생이 2명미만 || 남학생이 없다면 아예 만들 수 있는 팀이 없으므로 0 출력 
		while(girls >= 2 && boys >= 1 && boys + girls >= 3 + K) {
			girls -= 2;
			boys -= 1;
			
			teams++;
		}
		
		System.out.println(teams);
		
	}
}
